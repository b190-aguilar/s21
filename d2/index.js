console.log("Hello World!");

/*
	MINIACTIVITY
		create an array "cities" with the following elements
			Tokyo
			Manila
			Seoul
			Jakarta
			Sim

		log it in the console
		send the output in the chat
*/

let cities = ['Tokyo','Manila','Seoul','Jakarta','Sim'];
console.log(cities);
// check the number of elements in the array
console.log(cities.length);
// check the index of last element
console.log(cities.length - 1);
// print the last element of the array
console.log(cities[cities.length - 1]);


// in cases of blank arrays
let blankArr = [];
console.log(blankArr); //returns empty array
console.log(blankArr.length); //returns 0
console.log(blankArr[blankArr.length]); //returns undefined


// decrement operator can be used in the .length property to delete the LAST element of the array.
console.log(cities);
cities.length --;
console.log(cities);


let lakersLegends = ['Kobe','Shaq','Magic','Kareem','LeBron'];
console.log(lakersLegends);
	
// increment operator together with .length property adds an EMPTY element at the end of the array.
lakersLegends.length ++;
console.log(lakersLegends);

/*
	MINIACTIVITY
		replace the last element in the updated array with Pau Gasol using index
*/

lakersLegends[5] = "Gasol";
console.log(lakersLegends);

//would directly add a new element to the array
lakersLegends[lakersLegends.length] = "Davis"; 
console.log(lakersLegends);


let numArray = [5,12,30,46,40];
console.log(numArray);

for (let index = 0; index < numArray.length; index++) {
	if(numArray[index] % 5 === 0){
		console.log(numArray[index] + " is divisible by 5.")
	}

	else{
		console.log(numArray[index] + " is not divisible by 5.")
	}
};
/* same output, but using .forEach function
numArray.forEach(function(num){
	if(num % 5 === 0){
		console.log(num + " is divisible by 5.")
	}

	else{
		console.log(num + " is not divisible by 5.")
	}
});
*/


// print elements from first to last
for (let index = 0; index < numArray.length; index++){
	console.log(numArray[index]);
};
console.log("-----")

// print elements from last to first
for (let index = (numArray.length-1); index >= 0; index--){
	console.log(numArray[index]);
};


// Multidimensional Arrays
/*
	-these are useful for storing complex data structures
	-practical application of this it to help visualize/create real world objects
	-though useful in a number of cases, complex arrays is not always recommended.
*/

let chessboard = [
["a1",'b1','c1','d1','e1'],
["a2",'b2','c2','d2','e2'],
["a3",'b3','c3','d3','e3'],
["a4",'b4','c4','d4','e4'],
["a5",'b5','c5','d5','e5'],
];

console.log(chessboard);
// accessing elements of multidimensional arrays
console.log(chessboard[1][4]);	